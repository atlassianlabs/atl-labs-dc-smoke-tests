import os

import pytest
from selenium.webdriver.chrome.options import Options
from splinter import Browser

from test_confluence.confluence_utils import ConfluenceUtils


## Confluence specific configuration
#
def pytest_addoption(parser):
    parser.addoption(
        '--space',
        help='The pre-existing space to create test content in. Only specify this if you do not want a test space created.',
    )


# Inject the space key from the `--space` parameter into the session scope so it can be referenced in test functions
@pytest.fixture(scope='session', autouse=True)
def space(request):
    return request.config.getvalue('space') if request.config.getvalue('space') is not None else 'SMOKETEST'


# Create the Chrome browser and login if username and password were supplied
@pytest.fixture(scope='session', autouse=True)
def browser(service_url):
    # set browser options
    opts = Options()
    opts.add_argument("--no-sandbox")
    opts.add_argument("--disable-setuid-sandbox")
    opts.add_argument("--disable-software-rasterizer")
    opts.add_argument('--start-maximized')
    selenium_opts = {'options': opts}
    # create browser
    browser = Browser('chrome', headless=True, **selenium_opts)
    # attempt login if username/password specified
    username = os.getenv('SMOKE_TEST_USERNAME')
    password = os.getenv('SMOKE_TEST_PASSWORD')
    if username is not None and password is not None:
        browser.visit(f'{service_url}/login.action')
        if not ConfluenceUtils.login(browser, service_url, username, password):
            pytest.fail('Login was not successful')
    return browser
