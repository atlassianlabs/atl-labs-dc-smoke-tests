import pytest
from selenium.webdriver.common.keys import Keys

from common.utils import Utils


class ConfluenceUtils:
    @staticmethod
    def get_supported_versions():
        return [6, 7]

    @staticmethod
    def login(browser, service_url, username, password, retries=0):
        try:
            if not browser.status_code.is_success():
                pytest.fail('Login URL cannot be accessed')
        except NotImplementedError:
            # status code checking is not implemented in all web drivers
            pass
        if browser.title == 'Authorize app':
            browser.find_by_text('Accept').click()
        if browser.is_element_present_by_text('Continue with username and password'):
            native_auth_link = browser.find_by_xpath("//a[@data-authentication-method='native']").first['href']
            browser.visit(native_auth_link)
        if browser.is_element_present_by_id('os_username', wait_time=10):
            # normal Confluence login
            browser.find_by_id('os_username').fill(username)
            browser.find_by_id('os_password').fill(f'{password}{Keys.ENTER}')
            if browser.is_element_present_by_text('The following error(s) occurred:', wait_time=5):
                if '@' in username:
                    # if username is full email, try login with just prefix
                    if ConfluenceUtils.login(browser, service_url, username[:username.index('@')], password):
                        return True
                if browser.is_element_present_by_id('captcha-container'):
                    pytest.fail('Login requires Captcha, please resolve manually')
                pytest.fail('Username and password were incorrect')
            else:
                return True
        elif browser.is_element_present_by_id('username'):
            # bespoke CST auth login
            browser.find_by_id('username').fill(f'{username}{Keys.ENTER}')
            # wait for password field to display
            if not Utils.is_element_visible_by_id(browser, 'password', 5):
                pytest.fail('Password field was not visible within 5s')
            browser.find_by_id('password').fill(f'{password}{Keys.ENTER}')
            # check for errors
            if browser.is_element_present_by_id('login-error', wait_time=5):
                # if username is full email, try login with just prefix
                if '@' in username:
                    return ConfluenceUtils.login(browser, service_url, username[:username.index('@')], password)
            if browser.is_element_present_by_id('captcha-error'):
                pytest.fail('Login requires Captcha, please resolve manually')
            return True
        elif browser.is_element_present_by_text('Something went wrong.'):
            pytest.fail('Service URL cannot be accessed')
        elif browser.is_element_present_by_id('logout-link'):
            # you're already logged in
            return True
        elif browser.is_element_present_by_css('.k15t-plugin-attribution'):
            # if the service uses scroll viewport go direct to Confluence
            browser.visit(f'{service_url}/secure/Dashboard.jspa')
            if browser.is_element_present_by_id('logout-link'):
                # you're already logged in
                return True
            browser.find_by_id('login-link').click()
            return ConfluenceUtils.login(browser, service_url, username, password)
        else:
            # try again to avoid race conditions
            if retries == 0:
                if ConfluenceUtils.login(browser, service_url, username, password):
                    return True
        return False

    @staticmethod
    def get_latest_content_name(service_url, browser, space_key, content_type):
        browser.visit(
            f'{service_url}/dosearchsite.action?cql=space+%3D+"{space_key}"+and+type+%3D+"{content_type}"&queryString="Test+{content_type}"')
        if not browser.is_element_present_by_css('.search-result-meta', wait_time=10):
            # give the instance more time if needed and reload search results
            browser.visit(
                f'{service_url}/dosearchsite.action?cql=space+%3D+"{space_key}"+and+type+%3D+"{content_type}"&queryString="Test+{content_type}"')
        if not browser.is_element_present_by_css('.search-results-wrapper', wait_time=10):
            pytest.fail('Search page did not load in 10s')
        test_content_items = browser.find_by_xpath(f'//a[@data-type="{content_type}"]')
        timestamps = []
        for test_content_item in test_content_items._container:
            if test_content_item.text.startswith(f'Test {content_type} '):
                timestamps.append(int(test_content_item.text[test_content_item.text.rindex(' ') + 1:]))
        timestamps.sort()
        try:
            latest_content_item = f'Test {content_type} {timestamps[-1]}'
        except IndexError:
            pytest.skip(f'There are no test {content_type}s to delete')
        return latest_content_item

    # Type into the tinymce editor - works on pages, blogs and comments
    @staticmethod
    def type_into_editor(browser, content):
        if not browser.is_element_present_by_id('wysiwygTextarea_ifr', wait_time=20):
            pytest.fail(f'Editor did not fully load in 20s')
        with browser.get_iframe('wysiwygTextarea_ifr') as iframe:
            if not iframe.is_element_present_by_id('tinymce', wait_time=10):
                pytest.fail('Tinyme editor did not load in 10s')
            iframe.find_by_id('tinymce').fill(content)
