import os
import pytest
import time

from selenium.webdriver.common.keys import Keys
from test_confluence.confluence_utils import ConfluenceUtils
from common.utils import Utils


# Confluence tests that create content
# Requires the testing account to already exist in Confluence and have access to create spaces, pages, blogs and comments
# A preconfigured test space can be supplied with `--space=<KEY>` if desired
# Also requires SMOKE_TEST_USERNAME and SMOKE_TEST_PASSWORD set as environment variables


@pytest.mark.confluence
@pytest.mark.create
class TestConfluenceCreate:
    # Log the user out and test that the user can log in through the standard login page
    def test_login(self, service_url, browser):
        username = os.getenv('SMOKE_TEST_USERNAME')
        password = os.getenv('SMOKE_TEST_PASSWORD')
        if username is not None and password is not None:
            browser.visit(f'{service_url}/logout.action')
            browser.visit(f'{service_url}/login.action')
            if not browser.is_element_present_by_id('login-container', wait_time=10):
                pytest.fail('Login page did not load within 10s')
            browser.find_by_id('os_username').fill(username)
            browser.find_by_id('os_password').fill(password)
            browser.find_by_id('loginButton').click()
            login_link = browser.find_by_id('login-link')
            login_button = browser.find_by_id('loginButton')
            assert len(login_link) + len(login_button) == 0
            user_menu_link = browser.find_by_id('user-menu-link')
            assert len(user_menu_link) == 1

    # Create a test space for use in subsequent tests
    def test_create_space(self, service_url, browser, space):
        if space is not 'SMOKETEST':
            pytest.skip(f"An existing space key '{space}' was entered")
        browser.visit(f'{service_url}/display/{space}')
        if not browser.is_element_present_by_text("The page doesn't exist.", wait_time=5):
            pytest.skip('Smoke test space already exists')
        browser.visit(f'{service_url}/spaces/createspace-start.action')
        if not browser.is_element_present_by_name('name', wait_time=10):
            pytest.fail('Create space dialog did not progress within 10s')
        browser.find_by_name('name').type('Smoke Tests')
        backspaces = Keys.BACK_SPACE * 2
        key = f'{Keys.END}{backspaces}{space}'
        browser.find_by_name('key').type(key)
        # wait for space key to enter and 'Create' button to enable
        if not Utils.is_element_enabled_by_name(browser, 'create', 5):
            pytest.fail('Create button was not enabled within 5s')
        browser.find_by_name('create').click()
        # give the space time to create and to load the home page
        if not browser.is_element_present_by_text('Smoke Tests Home', wait_time=30):
            pytest.fail('Project was not created or board did not load within 30s')

    # Create a page in the test space
    def test_create_page(self, service_url, browser, space):
        page_title = f'Test page {int(time.time())}'
        page_content = 'Test page content'
        browser.visit(f'{service_url}/display/{space}')
        browser.click_link_by_id('quick-create-page-button')
        ConfluenceUtils.type_into_editor(browser, page_content)
        if not browser.is_element_present_by_id('content-title', wait_time=60):
            pytest.fail(f'Page title field did not load in 60s')
        browser.find_by_id('content-title').type(page_title)
        browser.click_link_by_id('rte-button-publish')
        if not browser.is_element_present_by_text(page_content, wait_time=30):
            pytest.fail('Page was not created or did not load within 30s')
        assert len(browser.find_by_text(page_title)) != 0
        assert len(browser.find_by_text(page_content)) != 0

    # Create a blog in the test space
    def test_create_blog(self, service_url, browser, space):
        blog_title = f'Test blogpost {int(time.time())}'
        blog_content = 'Test blogpost content'
        browser.visit(f'{service_url}/display/{space}')
        browser.click_link_by_id('create-page-button')
        if not browser.is_element_present_by_text('Blog post', wait_time=10):
            pytest.fail('Create dialog did not load in 10s')
        browser.execute_script("$('.template.selected').removeClass('selected')")
        browser.execute_script("$('li[data-item-module-complete-key=\"com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blog-post\"]').addClass('selected')")
        browser.find_by_css('.create-dialog-create-button').click()
        ConfluenceUtils.type_into_editor(browser, blog_content)
        if not browser.is_element_present_by_id('content-title', wait_time=60):
            pytest.fail(f'Blog title field did not load in 60s')
        browser.find_by_id('content-title').type(blog_title)
        browser.click_link_by_id('rte-button-publish')
        if not browser.is_element_present_by_text(blog_content, wait_time=30):
            pytest.fail('Blog was not created or did not load within 30s')
        assert len(browser.find_by_text(blog_title)) != 0
        assert len(browser.find_by_text(blog_content)) != 0

    # Comment on the test page
    def test_create_page_comment(self, service_url, browser, space):
        comment_content = 'Test comment'
        latest_page = ConfluenceUtils.get_latest_content_name(service_url, browser, space, 'page')
        browser.visit(f'{service_url}/display/{space}/{latest_page}')
        if not browser.is_element_present_by_css('.quick-comment-prompt', wait_time=10):
            pytest.fail('Page and quick comment section did not load within 10s')
        browser.find_by_css('.quick-comment-prompt').click()
        ConfluenceUtils.type_into_editor(browser, comment_content)
        browser.find_by_id('rte-button-publish').click()
        if not browser.is_element_present_by_id('page-comments', wait_time=10):
            pytest.fail('Comment did not save and display within 10s')
        assert browser.is_element_present_by_text(comment_content)

    # Comment on the test blog
    def test_create_blog_comment(self, service_url, browser, space):
        comment_content = 'Test comment'
        latest_blog = ConfluenceUtils.get_latest_content_name(service_url, browser, space, 'blogpost')
        browser.click_link_by_text(latest_blog)
        if not browser.is_element_present_by_css('.quick-comment-prompt', wait_time=10):
            pytest.fail('Blog and quick comment section did not load within 10s')
        browser.find_by_css('.quick-comment-prompt').click()
        ConfluenceUtils.type_into_editor(browser, comment_content)
        browser.find_by_id('rte-button-publish').click()
        if not browser.is_element_present_by_id('page-comments', wait_time=10):
            pytest.fail('Comment did not save and display within 10s')
        assert browser.is_element_present_by_text(comment_content)