import pytest

from test_confluence.confluence_utils import ConfluenceUtils


# Confluence tests


@pytest.mark.confluence
@pytest.mark.basic
class TestConfluenceBasic:
    # Check that 'All updates' view loads and has content
    def test_all_updates(self, service_url, browser):
        browser.visit(f'{service_url}/dashboard.action#all-updates')
        if not browser.is_element_present_by_css('.content-header-all-updates', wait_time=5):
            browser.visit(f'{service_url}/dashboard.action#all-updates')  # try to load the page again
            if not browser.is_element_present_by_css('.content-header-all-updates', wait_time=10):
                pytest.fail('All updates view did not load in 10s')
        if not browser.is_element_present_by_css('.update-item', wait_time=10):
            pytest.fail('All updates view loaded but no content was visible within 10s')

    # Check that the version number is displayed in the footer and that it is supported
    def test_version_in_footer(self, service_url, browser):
        browser.visit(f'{service_url}/dashboard.action#all-updates')
        if not browser.is_element_present_by_id('footer', wait_time=10):
            pytest.fail('Dashboard did not load in 10s')
        if not browser.is_element_present_by_id('footer-build-information', wait_time=10):
            pytest.fail(
                'Dashboard loaded but the footer did not contain build information or was not visible within 10s')
        major_version_from_footer = browser.find_by_id('footer-build-information').html[:1]
        try:
            if int(major_version_from_footer) not in ConfluenceUtils.get_supported_versions():
                pytest.fail(
                    f'Major version in footer is {major_version_from_footer} which may not be compatible with these tests.'
                    f'Supported versions are {ConfluenceUtils.get_supported_versions()}')
        except ValueError:
            pytest.fail(
                f"Footer does not appear to contain a version number. Found: {browser.find_element_by_id('footer-build-information').text}")

    # Check that the Space Directory loads and has at least one space
    def test_space_directory(self, service_url, browser):
        browser.visit(f'{service_url}/spacedirectory/view.action')
        if not browser.is_element_present_by_css('.space-search-title', wait_time=10):
            pytest.fail('Space Directory did not load in 10s')
        if not browser.is_element_present_by_css('.space-list-item', wait_time=10):
            pytest.fail('Space Directory loaded but no spaces were visible within 10s')

    # Check that the People Directory loads and has at least one user
    def test_people_directory(self, service_url, browser):
        browser.visit(f'{service_url}/browsepeople.action')
        if not browser.is_element_present_by_id('people-search', wait_time=10):
            pytest.fail('People Directory did not load in 10s')
        if not browser.is_element_present_by_css('.profile-macro', wait_time=10):
            pytest.fail('People Directory loaded but no people were visible within 10s')
