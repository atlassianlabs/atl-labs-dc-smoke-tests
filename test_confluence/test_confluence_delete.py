import os
import pytest
import time

from test_confluence.confluence_utils import ConfluenceUtils


# Confluence tests that delete the test content
# Requires the testing account to already exist in Confluence and have access to delete spaces, pages, blogs and comments
# A preconfigured test space can be supplied with `--space=<KEY>` if desired
# Also requires SMOKE_TEST_USERNAME and SMOKE_TEST_PASSWORD set as environment variables


@pytest.mark.confluence
@pytest.mark.delete
class TestConfluenceDelete:
    # Delete the comment on the test page
    def test_delete_page_comment(self, service_url, browser, space):
        latest_page = ConfluenceUtils.get_latest_content_name(service_url, browser, space, 'page')
        browser.click_link_by_text(latest_page)
        if not browser.is_element_present_by_text(latest_page, wait_time=10):
            pytest.fail('Page failed to load in 10s')
        comments = browser.find_by_xpath("//div[starts-with(@id, 'comment-')]")
        if len(comments) == 0:
            pytest.skip('There are no comments to delete')
        delete_link = browser.find_by_xpath("//a[starts-with(@id, 'remove-comment-')]")
        if len(delete_link) == 0:
            pytest.fail('Comment does not have a delete link')
        delete_link.click()
        browser.get_alert().accept()
        assert browser.is_element_not_present_by_id('page-comments', wait_time=10)

    # Delete the comment on the test blog
    def test_delete_blog_comment(self, service_url, browser, space):
        latest_blog = ConfluenceUtils.get_latest_content_name(service_url, browser, space, 'blogpost')
        browser.click_link_by_text(latest_blog)
        if not browser.is_element_present_by_text(latest_blog, wait_time=10):
            pytest.fail('Blog failed to load in 10s')
        comments = browser.find_by_xpath("//div[starts-with(@id, 'comment-')]")
        if len(comments) == 0:
            pytest.skip('There are no comments to delete')
        delete_link = browser.find_by_xpath("//a[starts-with(@id, 'remove-comment-')]")
        if len(delete_link) == 0:
            pytest.fail('Comment does not have a delete link')
        delete_link.click()
        browser.get_alert().accept()
        assert browser.is_element_not_present_by_id('page-comments', wait_time=10)

    # Delete the page created in the test space
    def test_delete_page(self, service_url, browser, space):
        time.sleep(5) # give the instance time to index the new page
        latest_page = ConfluenceUtils.get_latest_content_name(service_url, browser, space, 'page')
        browser.visit(f'{service_url}/display/{space}/{latest_page}')
        if not browser.is_element_present_by_text(latest_page, wait_time=10):
            pytest.fail('Page failed to load in 10s')
        browser.click_link_by_id('action-menu-link')
        if not browser.is_element_present_by_id('action-remove-content-link', wait_time=10):
            pytest.fail('Actions menu failed to display in 10s')
        browser.click_link_by_id('action-remove-content-link')
        if not browser.is_element_present_by_id('delete-dialog-next', wait_time=10):
            pytest.fail('Delete page dialog failed to display in 10s')
        browser.click_link_by_id('delete-dialog-next')
        if not browser.is_element_present_by_text('Page deleted', wait_time=10):
            pytest.fail('Page failed to delete in 10s')

    # Delete the blog created in the test space
    def test_delete_blog(self, service_url, browser, space):
        latest_blog = ConfluenceUtils.get_latest_content_name(service_url, browser, space, 'blogpost')
        browser.click_link_by_text(latest_blog)
        if not browser.is_element_present_by_text(latest_blog, wait_time=10):
            pytest.fail('Blog failed to load in 10s')
        browser.click_link_by_id('action-menu-link')
        if not browser.is_element_present_by_id('action-remove-content-link', wait_time=10):
            pytest.fail('Actions menu failed to display in 10s')
        browser.click_link_by_id('action-remove-content-link')
        if not browser.is_element_present_by_id('confirm', wait_time=10):
            pytest.fail('Delete blog dialog failed to display in 10s')
        browser.click_link_by_id('confirm')
        browser.visit(f'{service_url}/pages/viewrecentblogposts.action?key={space}')
        if not browser.is_element_present_by_id('watch-blog', wait_time=10):
            pytest.fail('Blog reel did not load within 10s')
        assert browser.is_element_not_present_by_text(latest_blog)

# Delete the test space if one was created
    def test_delete_space(self, service_url, browser, space):
        if space is not 'SMOKETEST':
            pytest.skip(f"An existing space key '{space}' was entered")
        browser.visit(f'{service_url}/spaces/removespace.action?key={space}')
        if browser.is_element_present_by_css('.websudo', wait_time=5):
            browser.find_by_id('password').fill(os.getenv('SMOKE_TEST_PASSWORD'))
            browser.find_by_id('authenticateButton').click()
        if not browser.is_element_present_by_text('Delete Space', wait_time=10):
            pytest.fail('Delete space page did not fully load in 10s')
        browser.click_link_by_id('confirm')
        if not browser.is_element_present_by_text('Deleted space', wait_time=20):
            pytest.fail('Space was not deleted within 20s')
