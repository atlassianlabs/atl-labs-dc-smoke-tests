import time

from splinter.exceptions import ElementDoesNotExist


class Utils:
    @staticmethod
    def is_element_visible_by_css(browser, css, wait_time):
        start_time = time.time()
        try:
            while not browser.find_by_css(css).visible:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False

    @staticmethod
    def is_element_visible_by_id(browser, id, wait_time):
        start_time = time.time()
        try:
            while not browser.find_by_id(id).visible:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False

    @staticmethod
    def is_element_visible_by_name(browser, name, wait_time):
        start_time = time.time()
        try:
            while not browser.find_by_name(name).visible:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False

    @staticmethod
    def is_element_visible_by_text(browser, text, wait_time):
        start_time = time.time()
        try:
            while not browser.find_by_text(text).visible:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False

    @staticmethod
    def is_element_enabled_by_css(browser, css, wait_time):
        start_time = time.time()
        try:
            while not browser.find_by_css(css)._element.is_enabled:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False

    @staticmethod
    def is_element_enabled_by_id(browser, id, wait_time):
        start_time = time.time()
        try:
            while 'disabled' in browser.find_by_id(id).first.outer_html:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False

    @staticmethod
    def is_element_enabled_by_name(browser, name, wait_time):
        start_time = time.time()
        try:
            while not browser.find_by_name(name)._element.is_enabled:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False

    @staticmethod
    def is_element_enabled_by_text(browser, text, wait_time):
        start_time = time.time()
        try:
            while not browser.find_by_text(text)._element.is_enabled:
                if (time.time() - start_time) > wait_time:
                    return False
                time.sleep(0.5)
            return True
        except ElementDoesNotExist:
            return False
