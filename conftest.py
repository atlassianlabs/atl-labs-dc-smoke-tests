import os
import subprocess
import sys
import time

import pytest
import requests
from webdrivermanager import ChromeDriverManager


## Common configuration for all test profiles
#
def pytest_addoption(parser):
    parser.addoption(
        '--url', help='The base url of the service to test', default='https://localhost',
    )


# Inject the service url from the `--url` parameter into the session scope so it can be referenced in test functions
@pytest.fixture(scope='session', autouse=True)
def service_url(request):
    return request.config.getvalue('url')


# Confirm the Chrome version on the machine and install the correct version of chromedriver once before all tests
def pytest_sessionstart():
    if sys.platform == 'darwin':
        result = subprocess.check_output(
            ['/Applications/Google Chrome.app/Contents/MacOS/Google Chrome', '--version']).decode()
    elif sys.platform == 'linux':
        result = subprocess.check_output(['google-chrome', '--version']).decode()
    else:
        pytest.fail('OS is not Linux or Mac, tests cannot be run')
    chrome_full_version = result.split()[2]
    version_to_query = chrome_full_version[:chrome_full_version.rindex('.')]
    url = f'https://chromedriver.storage.googleapis.com/LATEST_RELEASE_{version_to_query}'
    driver_version = requests.get(url).text
    ChromeDriverManager().download_and_install(driver_version)


# On test failure, write the browser object to an HTML file for troubleshooting
@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the result object
    outcome = yield
    result = outcome.get_result()
    # only look at actual failing test calls, not setup/teardown
    if result.when == 'call' and result.failed:
        if not os.path.exists('failure_artifacts'):
            os.makedirs('failure_artifacts')
        with open(f'failure_artifacts/{item.name}-browser-{int(time.time())}.html', 'w') as f:
            f.write(item.funcargs['browser'].html)
