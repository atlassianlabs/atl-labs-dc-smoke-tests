# Atlassian Smoke Tests for Jira and Confluence

NB: These tests are provided as-is and are NOT SUPPORTED by Atlassian

## Summary

These smoke tests are intended for use again Data Center deployments of Jira and Confluence. They use a simulated
browser to access the UI, and navigate through common functions.

They are written in python using pytest and Splinter (a wrapper for Selenium). They can be run via the command line, or
as part of a Bitbucket Pipelines build.

The tests can check basic reachability, and create and delete data, to test common functions.

By default the tests will create a space/project with key SMOKETEST. However, you can provide an existing space or
project key for the tests to use instead.

You can also decide which test suites and/or individual tests to run.

### Confluence Tests

* **Basic**: test the 'All updates' view loads and has content, check the version is compatible with these tests, check
  that the Space and People Directories load and display users/spaces
* **Create**: test login, create a space (if the default SMOKETEST space is used), create a page and a blog in that
  space, and comment on each
* **Delete**: delete the comments, the page and blog, and then the space (if the default SMOKETEST space is used)

### Jira Tests

* **Basic**: test for license warning in the footer, check the version is compatible with these tests, view all issues,
  and load the credits
* **Create**: create a project (if the default SMOKETEST project is used), create a ticket in that project, comment on
  the ticket
* **Delete**: delete the comment, ticket, and project (if the default SMOKETEST project is used)

### JSM Tests

* **Basic**: test that the plugin is enabled, and that the queue and portal views load

## Version compatability

Atlassian products change swiftly, so we can't guarantee that these tests will work against all versions. We intend to
target Enterprise releases.

Tested versions are as follows:

* Jira: `7.13.13`, `8.5.4`, `8.13.8`
* Confluence: `6.13.11`, `7.4.0`

## Pre-requisites

1. Install python 3
2. Install the python requirements with `pip3 install -r requirements.txt`
3. Install Chrome.

See [bitbucket-pipelines-setup.sh](https://bitbucket.org/atlassian/itops-smoke-tests/src/master/bitbucket-pipelines-setup.sh)
for how we configure the pipelines build agents.

## Required application configuration

### All tests

To run the create and delete tests, and to test a service that does not allow anonymous access, a user needs to exist in
the application with sufficient permissions for the test suites you want to run. The username and password must be
passed as environment variables: `SMOKE_TEST_USERNAME` and `SMOKE_TEST_PASSWORD`. If you run all test suites, the tests
will log in as the user, run the basic tests, then create content and delete what it created.

### Jira

To create a project in Jira the user must be a sysadmin, but we recognise that this might not be possible/desirable for
many customers. If you do not wish to run the tests as a sysadmin user, to run the create/delete suites you will need to
create a `Project Management` type project in Jira for the tests to use, and pass the project key in when calling
pytest, eg:
`pytest --url=http://localhost --project=MYTESTPROJ`.

If a project is specified, the tests will not try to create one, and when running delete, the project will _not_ be
removed (though the latest ticket and comment in that project will). As such, you should not specify a project that is
in active use. It should be a dedicated test project.

### Confluence

The user does not need to be a sysadmin, but must have permission to create spaces. This permission is granted to
confluence-users by default in a new installation.

If you wish to specify a preconfigured space to run the tests in, you will need to pass the space key in when calling
pytest, eg:
`pytest --url=http://localhost --space=MYTESTSPC`.

If a space is specified, the tests will not try to create one, and when running delete, the space will _not_ be
removed (though the latest page, blog and comment in that space will). As such, you should not specify a space that is
in active use. It should be a dedicated test space.

## Running the tests

The tests can be run by supplying the base URL of the instance to test against, and the product type as a pytest
mark: `pytest --url=http://localhost -m jira`.

This will run all test suites that are marked with `@pytest.mark.jira` - basic, create and delete.

## Running a subset of tests

We use `marks` to delineate test types. By default all test files marked with the product that you specify will be run,
but you can include/exclude test types or individual tests.

To run the basic tests only, run `pytest --url=http://localhost -m "jira and basic"`

To run all tests _except_ the delete tests, run `pytest --url=http://localhost -m "jira and not delete"`

To exclude two types of test tests, run `pytest --url=http://localhost -m "jira and (not create or delete)"`

## Running or skipping individual tests

Individual tests can be run or skipped in the usual pytest
format: `pytest --url=http://localhost test_jira_basic.py::test_credits`

To skip an individual test, run with:
`pytest --url=http://localhost --deselect test_jira/test_jira_basic.py::TestJiraBasic::test_credits`

Note the addition of the class name (eg TestJiraBasic) in the string.

Tests to skip can be daisy-chained by appending another `deselect`to the command, eg:
`pytest --url=http://localhost --deselect test_jira/test_jira_basic.py::TestJiraBasic::test_credits --deselect test_ira/test_jira_basic.py::TestJiraBasic::test_view_all_issues`

## Troubleshooting test failures

1. Look in failure_artifacts and open the corresponding HTML file in a browser. The failure may be immediately obvious
   here.
2. If you have access to an IDE that allows debugging:
    - Put a breakpoint at the failure
    - Investigate the `browser` object and compare with the expected results
    - Copying `browser.html` into a .html file and opening it with a browser can be helpful to see what is on the page
3. If you do NOT have access to an IDE, or this does not resolve the issue, follow the steps the failing tests takes
   manually in a browser:
    - Log in to the service with the test user account
    - If `browser.visit()` is used, browse to that URL
    - If the tests locate items by their ID/css/text, open your browser dev tools (eg F12) and search for the relevant
      string
    - If there are error messages relating to the user account and permissions etc, resolve them within the application
    - If a test fails due to a race condition (ie a test intermittently fails due to pages not loading fast enough etc),
      please raise a ticket in this repo to let us know

If you are having trouble finding the cause of test failures you can raise a ticket here as well. These tests are not
officially supported, but we will do best efforts support to try to help. However, it may be difficult for us to resolve
the issue as we would often need to see the full HTML of the page when the failure happens, which some customers may not
want to share.

## Development tips

### ElementClickInterceptedException

Sometimes the inbuilt Splinter browser functions don't work, you can get an ElementClickInterceptedException. This can
happen if the element is obscured by another element, which would instead receive the click.

For example, this code may trigger an exception:

`browser.find_by_css('.create-project-dialog-create-button').click()`

The more reliable way to interact with the element is by executing Javascript/JQuery directly:

`browser.execute_script('$(".template-info-dialog-create-button").click()')`

### Bootstrapping pytest

Pytest is bootstrapped in python's standard `conftest.py` files. There are three:

1. `conftest.py`
2. `test_confluence/conftest.py`
3. `test_jira/conftest.py`

The main `conftest.py` in the root of the repo defines common pytest command options (eg `--url`), installs
chromedriver, and creates a hook that saves `failure_artifacts` (the html of the current view in selenium) for failed
tests.

The two products `conftest.py` files extend this by adding product specific pytest command options, creating browsers
and logging the user in if usernames/passwords are supplied.

The fixtures with session scope create objects that can be accessed by all functions in the session, by referencing them
in the function constructor.

For example:

```
@pytest.fixture(scope='session', autouse=True)
def project(request):
    return request.config.getvalue('project') if request.config.getvalue(
        'project') is not None else 'SMOKETEST'
```

The project object is then referenced in the constructor of each test that requires it, eg:

```
def test_create_project(self, service_url, browser, project):
   do_thing()
```
