import pytest

from common import Utils
from test_jira.jira_utils import JiraUtils


# Jira tests


@pytest.mark.jira
@pytest.mark.basic
class TestJiraBasic:
    # Check that there is not a warning regarding the license in the footer
    def test_license_warning_in_footer(self, service_url, version, browser):
        if version not in (JiraUtils.get_supported_versions()):
            pytest.skip(f'No test condition exists for version {version}')
        browser.visit(f'{service_url}/secure/Dashboard.jspa')
        if len(browser.find_by_id('footer').first.find_by_css('.licensemessagered')) != 0:
            pytest.fail('There is a warning regarding the license in the footer')
        else:
            assert len(browser.find_by_id('footer').first.find_by_css('.licensemessage').text) == 0

    # Check that the version number is displayed in the footer and that it is supported
    def test_version_in_footer(self, service_url, browser):
        browser.visit(f'{service_url}/secure/Dashboard.jspa')
        footer = browser.find_by_id("footer-build-information").first
        footer_version = footer.value.split('#')[0]
        assert '(v' in footer_version
        if int(footer_version[2:3]) not in JiraUtils.get_supported_versions():
            pytest.fail(f'Major version in footer is {footer_version[2:3]} which may not be compatible with these tests.'
                        f'Supported versions are {JiraUtils.get_supported_versions()}')

    # Load the 'All issues' filter and confirms more than 5 are returned
    # 5 is a relatively safe number to confirm the index is ok while still allowing for smaller instances
    # or those where the user (or anonymous) has little access
    # First checks against detail view, then list view
    def test_view_all_issues(self, service_url, browser):
        # browse to 'All issues' filter
        browser.visit(f'{service_url}/issues/?filter=-4')
        if not browser.is_element_present_by_css('.issuerow', wait_time=10):
            if not browser.is_element_present_by_css('.issue-content-container'):
                pytest.fail('Search results did not display in either list or detail view in 10s')
        issues_found = browser.find_by_css('.issuerow')  # detail view
        if len(issues_found) == 0:
            issues_found = browser.find_by_css('.issue-content-container')  # list view
        assert len(issues_found) > 5

    # Confirm that the Jira credits can be accessed
    def test_credits(self, service_url, version, browser):
        if version not in (JiraUtils.get_supported_versions()):
            pytest.skip(f'No test condition exists for version {version}')
        browser.visit(f'{service_url}/secure/AboutPage.jspa')
        if not browser.is_element_present_by_text('Roll credits...', wait_time=10):
            pytest.fail('About page did not load in 10s')
        browser.click_link_by_partial_href('JiraCreditsPage')
        if version == 7:
            assert len(browser.find_by_id('jira-credits-home-button-game')) != 0
        elif version == 8:
            assert browser.is_text_present('Geoff Jacobs')
