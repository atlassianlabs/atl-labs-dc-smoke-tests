import os

import pytest

from common.utils import Utils
from test_jira.jira_utils import JiraUtils


# Jira tests that delete the test content
# Requires the testing account to already exist in Jira and have access to delete projects, issues and comments
# A preconfigured test project can be supplied with `--project=<KEY>` if desired
# Also requires SMOKE_TEST_USERNAME and SMOKE_TEST_PASSWORD set as environment variables


@pytest.mark.jira
@pytest.mark.delete
class TestJiraDelete:
    # Delete the comment created on the test issue
    def test_delete_comment(self, service_url, browser, project):
        issue_key = JiraUtils.get_latest_issue_key(service_url, browser, project)
        if not issue_key:
            pytest.skip(f"No issue created by smoke tests was found")
        browser.visit(f'{service_url}/browse/{issue_key}')
        if not browser.is_element_present_by_id('footer-comment-button', wait_time=10):
            pytest.fail('Issue did not fully load in 10s')
        if browser.is_element_not_present_by_css('.delete-comment', wait_time=10):
            pytest.skip('There is no comment to be deleted')
        browser.execute_script('$(".delete-comment").click()')
        if not browser.is_element_present_by_id('comment-delete-submit', wait_time=10):
            pytest.fail('Delete comment confirmation did not appear within 10s')
        browser.execute_script('$("#comment-delete-submit").click()')
        if not browser.is_element_not_present_by_css('.delete-comment', wait_time=10):
            pytest.fail('The comment was not deleted within 10s')
        assert len(browser.find_by_xpath(
            "//div[starts-with(@id, 'comment-') and not(contains(@id, 'comment-wiki-edit'))]")) == 0

    # Delete the created issue in the test project
    def test_delete_issue(self, service_url, browser, project):
        issue_key = JiraUtils.get_latest_issue_key(service_url, browser, project)
        if not issue_key:
            pytest.skip(f"No issue created by smoke tests was found")
        browser.visit(f'{service_url}/browse/{issue_key}')
        browser.find_by_id('opsbar-operations_more').click()
        delete_issue_link = browser.find_by_xpath("//a[contains(@href, 'DeleteIssue!default.jspa?id=')]").first['href']
        browser.visit(delete_issue_link)
        if not browser.is_element_present_by_id('delete-issue-submit', wait_time=10):
            pytest.fail('Delete issue confirmation did not appear within 10s')
        browser.find_by_id('delete-issue-submit').click()
        assert len(browser.find_by_value(issue_key)) == 0

    # Delete the test project if one was created
    def test_delete_project(self, service_url, browser, project):
        if project != 'SMOKETEST':
            pytest.skip(f"An existing project key '{project}' was entered")
        browser.visit(f'{service_url}/browse/{project}')
        if Utils.is_element_visible_by_text(browser, "You can't view this project", wait_time=5):
            pytest.skip('Smoke test project does not appear to exist')
        browser.visit(f'{service_url}/plugins/servlet/project-config/{project}/summary')
        if not browser.is_element_present_by_id('view_delete_project', wait_time=10):
            pytest.fail('Delete project link in project config was not displayed within 30s')
        delete_project_link = browser.find_by_xpath("//a[contains(@href, 'DeleteProject!default.jspa?pid=')]").first[
            'href']
        browser.visit(delete_project_link)
        if len(browser.find_by_name('webSudoPassword')) != 0:
            browser.find_by_name('webSudoPassword').fill(os.getenv('SMOKE_TEST_PASSWORD'))
            browser.execute_script('$("#login-form-submit").click()')
            browser.execute_script('$("#delete-project-confirm-submit").click()')
        # extra confirmation step in Jira 8.13
        if browser.is_element_present_by_id('delete-project-confirm-submit', wait_time=30):
            browser.execute_script('$("#delete-project-confirm-submit").click()')
        if not browser.is_element_present_by_id('acknowledge-submit', wait_time=30):
            pytest.fail('Project was not deleted within 30s')
        browser.execute_script('$("#acknowledge-submit").click()')
        # assert you're back on the browse projects page
        assert browser.is_element_present_by_id('browse-projects-page', wait_time=10)
