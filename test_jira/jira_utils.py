import re

import pytest
from selenium.webdriver.common.keys import Keys
from splinter.exceptions import ElementDoesNotExist

from common import Utils


class JiraUtils:
    @staticmethod
    def get_supported_versions():
        return [7, 8]

    @staticmethod
    def login(browser, service_url, username, password, retries=0):
        try:
            if not browser.status_code.is_success():
                # login URL cannot be accessed
                return False
        except NotImplementedError:
            # status code checking is not implemented in all web drivers
            pass
        if browser.title == 'Authorize app':
            browser.find_by_text('Accept').click()
        if browser.is_element_present_by_id('login-form-username', wait_time=10):
            # normal Jira login
            browser.find_by_id('login-form-username').fill(username)
            browser.find_by_id('login-form-password').fill(password)
            if browser.is_element_present_by_id('login-form-submit'):
                browser.find_by_id('login-form-submit').click()
                if browser.is_element_present_by_id('usernameerror', wait_time=2):
                    # if username is full email, try login with just prefix
                    if '@' in username and \
                            JiraUtils.login(browser, service_url, username[:username.index('@')], password):
                        return True
                    pytest.fail('Username and password were incorrect')
                    if browser.is_element_present_by_id('captcha-error'):
                        pytest.fail('Login requires Captcha, please resolve manually')
                return True
            elif browser.is_element_present_by_id('login'):
                browser.find_by_id('login').click()
                return True
        elif browser.is_element_present_by_css('.centrify-login'):
            # centrify login - can start at username, password or 2fa prompt depending on cached data
            if browser.find_by_id('usernameForm').visible:
                username_without_email = username[:username.index('@')] if '@' in username else username
                browser.find_by_name('username').fill(f'{username_without_email}{Keys.ENTER}')
                # wait for password field to display
                if not Utils.is_element_visible_by_id(browser, 'passwordForm', 5):
                    pytest.fail('Password field was not visible within 5s')
                browser.find_by_name('answer').fill(f'{password}{Keys.ENTER}')
            # wait for 2fa prompt to display
            if Utils.is_element_visible_by_id(browser, 'mechanismSelectionForm', 5):
                mfa_field = browser.find_by_xpath("//div[@id='mechanismSelectionForm']//input[@name='mfaAnswer']")
                mfa_field.fill(f'push{Keys.ENTER}')
            elif browser.is_element_present_by_text(
                    'User does not have the attributes required to login. Please contact your administrator.'):
                pytest.fail('Centrify error: user does not have the attributes required to login')
            else:
                pytest.fail('Auth dance did not get to 2fa prompt')
            # load Jira dashboard and wait
            browser.visit(f'{service_url}/secure/Dashboard.jspa')
            if not browser.is_element_present_by_id('jira', wait_time=80):
                if browser.is_element_present_by_css('.error-message'):
                    # if username is full email, try login with just prefix
                    if '@' in username and \
                            JiraUtils.login(browser, service_url, username[:username.index('@')], password):
                        return True
                    pytest.fail('Username and password were incorrect')
                pytest.fail('2FA push request was not actioned or Jira did not load')
            return True
        elif browser.is_element_present_by_id('username'):
            # bespoke CST auth login
            browser.find_by_id('username').fill(f'{username}{Keys.ENTER}')
            if not Utils.is_element_visible_by_id(browser, 'password', 5):
                pytest.fail('Password field was not visible within 5s')
            browser.find_by_id('password').fill(f'{password}{Keys.ENTER}')
            if browser.is_element_present_by_id('login-error', wait_time=5):
                # if username is full email, try login with just prefix
                if '@' in username:
                    return JiraUtils.login(browser, service_url, username[:username.index('@')], password)
                return False
            return True
        elif browser.is_element_present_by_text('Something went wrong.'):
            pytest.fail('Service URL cannot be accessed')
        elif browser.is_element_present_by_id('log_out'):
            # you're already logged in
            return True
        else:
            # try to access the dashboard and check status
            browser.visit(f'{service_url}/secure/Dashboard.jspa')
            if browser.is_element_present_by_id('log_out'):
                # you're already logged in
                return True
            # try again to avoid race conditions
            if retries == 0:
                if JiraUtils.login(browser, service_url, username, password, retries=1):
                    return True
        return False

    @staticmethod
    def get_latest_issue_key(service_url, browser, project):
        browser.visit(f'{service_url}/projects/{project}/issues/?filter=allopenissues&orderby=created+DESC')
        try:
            latest_test_ticket_li = browser.find_by_xpath('//li[@title="Test issue from smoke tests"]').first.outer_html
        except ElementDoesNotExist:
            return False
        return re.match(f"^.*(?P<key>{project}-[0-9]+).*$", latest_test_ticket_li).group('key')
