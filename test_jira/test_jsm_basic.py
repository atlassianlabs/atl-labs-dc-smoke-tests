import json
import os

import pytest
import requests

from test_jira.jira_utils import JiraUtils


# Jira Service Management tests
# These will check the plugin is enabled, and test the portal and agent queue views
# A user with the agent role is required


@pytest.mark.jsm
@pytest.mark.basic
class TestJSMBasic:
    # Check the plugin is enabled
    def test_plugin_enabled(self, service_url, version, browser):
        if version not in (JiraUtils.get_supported_versions()):
            pytest.skip(f'No test condition exists for version {version}')
        response = requests.get(f'{service_url}/rest/plugins/1.0/com.atlassian.servicedesk-key',
                                auth=(os.getenv('SMOKE_TEST_USERNAME'), os.getenv('SMOKE_TEST_PASSWORD')))
        if response.status_code != 200:
            # The full username with domain is required for Atlassian ID login, but not for API calls
            # Try again, cutting the domain out of the username (this will not fail if there is no domain)
            response = requests.get(f'{service_url}/rest/plugins/1.0/com.atlassian.servicedesk-key',
                                    auth=(
                                        os.getenv('SMOKE_TEST_USERNAME').split("@")[0],
                                        os.getenv('SMOKE_TEST_PASSWORD')))
        assert json.loads(response.text)['enabled'] == True

    # Check that the JSM queue view loads
    def test_queues(self, service_url, version, browser, jsm_project):
        if version not in (JiraUtils.get_supported_versions()):
            pytest.skip(f'No test condition exists for version {version}')
        browser.visit(f'{service_url}/projects/{jsm_project}/queues/')
        if not browser.is_element_present_by_text('Queues', wait_time=10):
            pytest.fail('Page failed to load in 10s')

    # Test the JSM portal loads
    def test_portal(self, service_url, version, browser, jsm_project):
        if version not in (JiraUtils.get_supported_versions()):
            pytest.skip(f'No test condition exists for version {version}')
        browser.visit(f'{service_url}/servicedesk/customer/portal')
        if not browser.find_by_css('.jsd-footer', wait_time=10):
            pytest.fail('Customer portal view failed to load in 10s')
