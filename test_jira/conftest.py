import os

import pytest
from selenium.webdriver.chrome.options import Options
from splinter import Browser

from test_jira.jira_utils import JiraUtils


## Jira specific configuration

def pytest_addoption(parser):
    parser.addoption(
        '--project',
        help='The pre-existing project to create test content in. Only specify this if you do not want a test project created.'
             "Must be 'Project Management' project type.",
    )
    parser.addoption(
        '--jsm-project', help='The JSM project to test'
    )


# Inject parameters from `pytest_addoption()` in this conftest.py
# into the session scope so they be referenced in test functions
@pytest.fixture(scope='session', autouse=True)
def project(request):
    return request.config.getvalue('project') if request.config.getvalue(
        'project') is not None else 'SMOKETEST'


@pytest.fixture(scope='session', autouse=True)
def jsm_project(request):
    return request.config.getvalue('jsm_project')


# Create the Chrome browser and login if username and password were supplied
@pytest.fixture(scope='session', autouse=True)
def browser(service_url):
    # set browser options
    opts = Options()
    opts.add_argument("--no-sandbox")
    opts.add_argument("--disable-setuid-sandbox")
    opts.add_argument("--disable-software-rasterizer")
    opts.add_argument('--start-maximized')
    selenium_opts = {'options': opts}
    # create browser
    browser = Browser('chrome', headless=True, **selenium_opts)
    # attempt login if username/password specified
    username = os.getenv('SMOKE_TEST_USERNAME')
    password = os.getenv('SMOKE_TEST_PASSWORD')
    if username is not None and password is not None:
        browser.visit(f'{service_url}/login.jsp')
        if not JiraUtils.login(browser, service_url, username, password):
            browser.visit(f'{service_url}/secure/Dashboard.jspa')
            if not JiraUtils.login(browser, service_url, username, password):
                pytest.fail('Login was not successful')
    return browser


# Scrape the product version from the footer and inject it into the session
@pytest.fixture(scope='session', autouse=True)
def version(service_url, browser):
    browser.visit(f'{service_url}/secure/Dashboard.jspa')
    if not browser.is_element_present_by_id('footer', wait_time=10):
        pytest.fail('Footer did not load within 10s. Login may have failed.')
    application_name_tag = browser.find_by_name('application-name').first.outer_html
    version = application_name_tag[application_name_tag.rindex('=') + 2:application_name_tag.rindex('\"')]
    return int(version[:1])
