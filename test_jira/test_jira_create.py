import os

import pytest
from selenium.webdriver.common.keys import Keys

from common.utils import Utils
from test_jira.jira_utils import JiraUtils


# Jira tests that create content
# Requires the testing account to already exist in Jira and have access to create projects, issues and comments
# A preconfigured test project of type 'Project management' can be supplied with `--project=<KEY>` if desired
# Also requires SMOKE_TEST_USERNAME and SMOKE_TEST_PASSWORD set as environment variables


@pytest.mark.jira
@pytest.mark.create
class TestJiraCreate:
    # Test that the user can log in through the standard login page
    def test_login(self, service_url, browser):
        username = os.getenv('SMOKE_TEST_USERNAME')
        password = os.getenv('SMOKE_TEST_PASSWORD')
        if username is not None and password is not None:
            browser.visit(f'{service_url}/login.jsp')
            if not JiraUtils.login(browser, service_url, username, password):
                browser.visit(f'{service_url}/secure/Dashboard.jspa')
                if not JiraUtils.login(browser, service_url, username, password):
                    pytest.fail('Login was not successful')

    # Create a test project for use in subsequent tests
    def test_create_project(self, service_url, browser, project):
        if project != 'SMOKETEST':
            pytest.skip(f"An existing project key '{project}' was entered")
        browser.visit(f'{service_url}/browse/{project}')
        if not browser.is_element_present_by_text("You can't view this project", wait_time=5):
            pytest.skip('Smoke test project already exists')
        browser.visit(f'{service_url}/secure/BrowseProjects.jspa?selectedCategory=all&selectedProjectType=all')
        browser.click_link_by_id('browse_link')
        if not browser.is_element_present_by_id('project_template_create_link_lnk', wait_time=10):
            pytest.fail(
                f"Browse projects dropdown did not appear within 10s, or user {os.getenv('SMOKE_TEST_USERNAME')} does not have permission to create projects")
        browser.click_link_by_id('project_template_create_link_lnk')
        if not browser.is_element_present_by_css('.create-project-dialog-create-button', wait_time=10):
            pytest.fail('Create project dialog not visible in 10s')
        browser.execute_script("$('.template.selected').removeClass('selected')")
        browser.execute_script("$('li[data-name=\"Project management\"]').addClass('selected')")
        browser.execute_script('$(".create-project-dialog-create-button").click()')
        browser.execute_script('$(".template-info-dialog-create-button").click()')
        if not browser.is_element_present_by_id('name', wait_time=10):
            pytest.fail('Create project modal not present within 10s')
        browser.find_by_id('name').fill('Smoke test')
        backspaces = Keys.BACK_SPACE * 2
        key = f'{Keys.END}{backspaces}{project}'
        browser.find_by_id('key').fill(key)
        # race condition here means sometimes the key is overwritten with the default 'ST'
        # wait for the correct key to be present; if it's not within 10s, delete 'ST' and write it again
        if not browser.is_element_present_by_value(f'{project}', wait_time=10):
            browser.find_by_id('key').fill(key)
        browser.execute_script('$(".add-project-dialog-create-button").click()')
        # give the project time to create and to load the board
        if not browser.is_element_present_by_css('.project-management-empty-state', wait_time=30):
            pytest.fail('Project was not created or board did not load within 30s')

    # Create an issue in the test project
    def test_create_issue(self, service_url, browser, project):
        browser.visit(f'{service_url}/browse/{project}')
        browser.visit(f'{service_url}/secure/CreateIssue!default.jspa')
        if not browser.is_element_present_by_id('project-field', wait_time=10):
            pytest.fail(f'Issue create form did not fully load in 10s')
        browser.find_by_id('project-field').type(f'{project}{Keys.ENTER}', slowly=True)
        if browser.is_element_present_by_text('No Matches', wait_time=1) and \
                Utils.is_element_visible_by_text(browser, 'No Matches', wait_time=1):
            pytest.fail(f'Cannot find project {project}')
        browser.execute_script('$("#issue-create").submit()')
        if not browser.is_element_present_by_id('summary', wait_time=10):
            pytest.fail(f'Issue detail form did not fully load in 10s')
        browser.find_by_id('summary').fill('Test issue from smoke tests')
        browser.execute_script('$("#issue-create-submit").click()')
        if not browser.is_element_present_by_text('Click to add description', wait_time=10):
            pytest.fail('Issue was not created within 10s')
        assert f'{project}-'.lower() in browser.find_by_id('key-val').text.lower()

    # Add a comment on the test issue
    def test_add_comment(self, service_url, browser, project):
        issue_key = JiraUtils.get_latest_issue_key(service_url, browser, project)
        if not issue_key:
            pytest.skip(f"No issue created by smoke tests was found")
        browser.visit(f'{service_url}/browse/{issue_key}')
        if not browser.is_element_present_by_id('footer-comment-button', wait_time=10):
            pytest.fail(f'Issue did not fully load in 10s')
        assert len(browser.find_by_xpath(
            "//div[starts-with(@id, 'comment-') and not(contains(@id, 'comment-wiki-edit'))]")) == 0
        browser.execute_script('$("#footer-comment-button").click()')
        # click visual editing mode if it exists (Jira 8.13)
        visual_mode_elem = browser.find_by_xpath('//li[@data-mode="wysiwyg"]')
        if len(visual_mode_elem) > 0:
            visual_mode_elem.click()
        # get the tinyme comment editor from the iframe and add the comment
        if browser.is_element_present_by_id('mce_0_ifr', wait_time=10):
            with browser.get_iframe('mce_0_ifr') as iframe:
                if not iframe.is_element_present_by_id('tinymce', wait_time=10):
                    pytest.fail(f'Comment editor did not load in 10s')
                iframe.find_by_id('tinymce').fill('Test comment')
        elif browser.is_element_present_by_id('mce_8_ifr', wait_time=1):
            with browser.get_iframe('mce_8_ifr') as iframe:
                if not iframe.is_element_present_by_id('tinymce', wait_time=10):
                    pytest.fail(f'Comment editor did not load in 10s')
                iframe.find_by_id('tinymce').fill('Test comment')
        else:
            pytest.fail(f'Comment editor did not fully load in 10s')
        # wait for comment to enter and 'Add' button to enable
        if not Utils.is_element_enabled_by_id(browser, 'issue-comment-add-submit', 5):
            pytest.fail('Comment submit button was not enabled with 5s')
        browser.execute_script('$("#issue-comment-add-submit").click()')
        if not browser.is_element_present_by_css('.activity-comment', 10):
            pytest.fail('Comment was not added and displayed within 10s')
